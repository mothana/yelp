**Mothana's Yelp List app**

This is a basic Yelp app that I created to hit some specific goals.
* It had to use MVVM pattern (done)
* it had to use Kotlin (done)
* It had to use Retrofit (done)
* It had to use a ConstraintLayout(done) .. But i would never use a constraint layout in a list item unless the view is very complex. The current item is way to simple for the overhead

 Optional Items:
* Dependency Injection: The app is way to simple for Dagger. It only has one repo and service
* Multiple Recylcerview type (done) 
* Unit tests (Basic unit tests done)
* Integration tests: everything that the ui tests would cover is already covered in unit tests.

Extra notes :
*  I went wanted to keep as much of the Business Logic of the app in the ViewModel and the repo. e.g The RecyclerView adapter is as bear bones as possible. The idea behind this is to allow for more unit tests coverage and less Ui tests coverage.
*  I attempted to treat the project from a CI/CD perspective as a Legit project, so I created branches for each new "feature" and made sure that "all" the unit tests passed before i merged in the PR.


package com.example.yelpApp.viewModel

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.yelpApp.repo.Repo
import com.example.yelpApp.repo.RepoImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
    private var repo: Repo = RepoImpl()
    var list = MutableLiveData<List<adapterModel>>()
    var error = MutableLiveData<Boolean>()

    fun getBusiness() {
        viewModelScope.launch(Dispatchers.IO)
        {
            try {
                val tempList = repo.getBusiness().sortedByDescending { it.reviewCount }.groupBy { it.reviewCount }.map {
                    val amount = it.key
                    val list = ArrayList<adapterModel>()
                    list.add(adapterModel(adapterType.HEADER, null, "Review Count : $amount"))
                    for (yelpSearchDomainModel in it.value) {
                        list.add(adapterModel(adapterType.YELPITEM, yelpSearchDomainModel, null))
                    }
                    list
                }.flatten()
                list.postValue(tempList)

            } catch (e: Exception) {
                error.postValue(true)
            }
        }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun setRepo(_repo: Repo) {
        repo = _repo
    }
}
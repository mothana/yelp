package com.example.yelpApp.viewModel

import com.example.yelpApp.repo.yelp_search_domain_model

enum class adapterType {
    HEADER, YELPITEM
}

data class adapterModel(val type: adapterType, val item: yelp_search_domain_model?, val header: String?)
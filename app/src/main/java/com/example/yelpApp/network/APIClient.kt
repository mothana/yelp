package com.example.yelpApp.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class APIClient {
    val client: YelpService by lazy { Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl("https://api.yelp.com").build().create(YelpService::class.java) }
}

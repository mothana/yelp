package com.example.yelpApp.network

import com.example.yelpApp.repo.yelp_search_result
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query


interface YelpService {
    @GET("/v3/businesses/search")
    suspend fun getBusinesses(@Query("location") location: String, @Query("limit") limit: Int, @Query("categories") category: String, @Query("offset") offset: Int = 0,
                              @Header("Authorization") credentials: String = "Bearer sVVg7Cw5ktcRrJxpXgUkHdGzlWREMSW1rgUhA-hKWlQgjmwJqyS5S6av_RDZybt5MMpVObRuyGNOMDghOfHd3DR9yrtLWUmV71E8oA_ENgMZXUw2iTpXdHXZR4K3XXYx"): yelp_search_result
}
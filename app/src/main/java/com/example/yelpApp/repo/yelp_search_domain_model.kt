package com.example.yelpApp.repo

data class yelp_search_domain_model(val nameOfBusiness: String, val address: String, val reviewCount: Int)

package com.example.yelpApp.repo

interface Repo {
    suspend fun getBusiness(): List<yelp_search_domain_model>
}
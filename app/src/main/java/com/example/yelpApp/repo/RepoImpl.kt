package com.example.yelpApp.repo

import com.example.yelpApp.network.APIClient

class RepoImpl : Repo {
    var client = APIClient().client
    var offset = 0


    override suspend fun getBusiness(): List<yelp_search_domain_model> {
        offset = 0
        val response = client.getBusinesses("melbourne", 50, "coffee", 0)
        offset += response.total
        return response.businesses.map {
            yelp_search_domain_model(it.name
                    ?: "Unknown", convertAddress(it.location), it.reviewCount ?: 0)
        }


    }


    private fun convertAddress(location: Location?): String {
        if (location == null) return ""
        return "${location.address1.convertNullToEmpty()} ${location.address2.convertNullToEmpty()} \n ${location.address3.convertNullToEmpty()} ${location.city.convertNullToEmpty()}"
    }

    private fun Any?.convertNullToEmpty(): String = this?.toString() ?: ""
}
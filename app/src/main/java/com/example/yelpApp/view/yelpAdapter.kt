package com.example.yelpApp.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.yelpApp.R
import com.example.yelpApp.repo.yelp_search_domain_model
import com.example.yelpApp.viewModel.adapterModel
import com.example.yelpApp.viewModel.adapterType

class yelpAdapter(private var yelpItems: List<adapterModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    fun updateListem(_yelpItems: List<adapterModel>) {
        yelpItems = _yelpItems
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
            if (viewType == 0) {
                HeaderViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.yelp_header_list_item, parent, false))
            } else {
                YelpViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.yelp_list_item, parent, false))
            }

    override fun getItemCount() = yelpItems.size


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is YelpViewHolder -> holder.bind(yelpItems[position].item
                    ?: yelp_search_domain_model("error", "Unknown", -1))
            is HeaderViewHolder -> holder.bind(yelpItems[position].header ?: "-1")
            else -> throw IllegalStateException()
        }

    }

    override fun getItemViewType(position: Int) = if (yelpItems[position].type == adapterType.HEADER) 0 else 1


    class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val header: TextView? = itemView.findViewById(R.id.header)

        fun bind(ammount: String) {
            header?.text = ammount
        }

    }

    class YelpViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val address: TextView? = itemView.findViewById(R.id.address_value)
        private val name: TextView? = itemView.findViewById(R.id.name)
        private val reviews: TextView? = itemView.findViewById(R.id.review_value)


        fun bind(yelpSearchResult: yelp_search_domain_model) {
            address?.text = yelpSearchResult.address
            name?.text = yelpSearchResult.nameOfBusiness
            reviews?.text = yelpSearchResult.reviewCount.toString()
        }

    }
}
package com.example.yelpApp

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.yelpApp.view.yelpAdapter
import com.example.yelpApp.viewModel.MainViewModel

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private lateinit var model: MainViewModel
    private var adapter: yelpAdapter = yelpAdapter(emptyList())
    private lateinit var recylerview: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        recylerview = findViewById(R.id.recylerView)
        recylerview.layoutManager = LinearLayoutManager(this)
        recylerview.adapter = adapter
        model = ViewModelProviders.of(this).get(MainViewModel::class.java)
        model.getBusiness()
        model.list.observe(this, Observer {
            adapter.updateListem(it)
        })
        model.error.observe(this, Observer {
            Toast.makeText(this, "error", Toast.LENGTH_LONG).show()
        })
    }


}


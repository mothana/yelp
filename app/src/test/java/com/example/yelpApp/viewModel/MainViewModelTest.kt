package com.example.yelpApp.viewModel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.*
import com.example.yelpApp.repo.Repo
import com.example.yelpApp.repo.yelp_search_domain_model
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.Test

class MainViewModelTest {


    @get:Rule
    val rule = InstantTaskExecutorRule()


    @Test
    fun successfulRun() {
        val model = MainViewModel()
        val mockRepo = mockRepo()
        mockRepo.success = true
        model.setRepo(mockRepo)

        runBlocking { model.getBusiness() }

        model.list.observeOnce {
            assertEquals("Review Count : 12", it[0].header)
            assertEquals(adapterType.HEADER, it[0].type)
            assertEquals("123", it[1].item?.nameOfBusiness)
            assertEquals("1",it[1].item?.address)
            assertEquals("12", it[1].item?.reviewCount)
            assertEquals(adapterType.YELPITEM, it[1].type)
            assertEquals(4, it.size)
        }
        model.error.observeOnce { assertEquals(null, it) }

    }


    @Test
    fun errorState() {
        val model = MainViewModel()
        val mockRepo = mockRepo()
        mockRepo.success = false
        model.setRepo(mockRepo)

        runBlocking { model.getBusiness() }

        model.list.observeOnce {
            assertEquals(null, it)
        }
        model.error.observeOnce { assertEquals(true, it) }

    }


    private fun <T> LiveData<T>.observeOnce(onChangeHandler: (T) -> Unit) {
        val observer = OneTimeObserver(handler = onChangeHandler)
        observe(observer, observer)
    }

    class mockRepo : Repo {
        var success = false
        override suspend fun getBusiness(): List<yelp_search_domain_model> {
            return if (success) listOf(yelp_search_domain_model("123", "1", 12), yelp_search_domain_model("123", "1", 10)) else throw Exception()
        }

    }


    class OneTimeObserver<T>(private val handler: (T) -> Unit) : Observer<T>, LifecycleOwner {
        private val lifecycle = LifecycleRegistry(this)

        init {
            lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
        }

        override fun getLifecycle(): Lifecycle = lifecycle

        override fun onChanged(t: T) {
            handler(t)
            lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        }
    }

}
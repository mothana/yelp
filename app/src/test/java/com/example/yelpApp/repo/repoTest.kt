package com.example.yelpApp.repo

import com.example.yelpApp.network.YelpService
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


@RunWith(JUnit4::class)
class repoTest {

    @Test
    fun getSuccessfulBusinessCall() {
        runBlocking {
            val repo = RepoImpl()
            repo.client = MockYelpService()
            val response = repo.getBusiness()

            Assert.assertEquals("1  \n" +
                    " null st Melbourne", response.first().address)
            Assert.assertEquals("test", response.first().nameOfBusiness)
            Assert.assertEquals(12, response.first().reviewCount)

        }

    }


    @Test
    fun BadBusinessCall() {
        runBlocking {

            val repo = RepoImpl()
            val webService = MockYelpService()
            webService.succeful = false
            repo.client = webService
            val response = repo.getBusiness()
            Assert.assertEquals("", response.first().address)
            Assert.assertEquals("Unknown", response.first().nameOfBusiness)
            Assert.assertEquals(0, response.first().reviewCount)
        }

    }


    class MockYelpService : YelpService {
        var succeful = true

        override suspend fun getBusinesses(location: String, limit: Int, category: String, offset: Int, credentials: String): yelp_search_result {
            if (succeful) return mockSuccesfullRespone()
            else return mockFailingRespone()
        }

        private fun mockFailingRespone(): yelp_search_result {
            return yelp_search_result(listOf(Businesse("", listOf(), Coordinates(1.0, 1.0), 12.0, "", "", false,
                    null, null, "123", "11", 5.0f, null, emptyList(), "")), Region(Center(1.0, 1.0)), 1)
        }

        fun mockSuccesfullRespone(): yelp_search_result {
            return yelp_search_result(listOf(Businesse("", listOf(), Coordinates(1.0, 1.0), 12.0, "", "", false,
                    Location("1", "", "null st", "Melbourne", "aus", "vic", "1"),
                    "test", "123", "11", 5.0f, 12, emptyList(), "")), Region(Center(1.0, 1.0)), 1)
        }

    }


}